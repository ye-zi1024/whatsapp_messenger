class ResponseModel {
  String? status;
  String? msg;
  Object? data;

  ResponseModel({this.status, this.msg, this.data});

  ResponseModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    data = json['data'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    data['data'] = this.data;
    return data;
  }

  factory ResponseModel.fromMap(Map<String, dynamic> map) {
    return ResponseModel(
      status: map['status'] ?? '',
      msg: map['msg'] ?? '',
      data: map['data'] ?? '',
    );
  }
}
