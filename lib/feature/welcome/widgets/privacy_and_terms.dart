
import 'package:flutter/material.dart';
import 'package:whatsapp_messenger/common/extension/custom_theme_extension.dart';

class PrivacyAndTerms extends StatelessWidget {
  const PrivacyAndTerms({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 30,
        vertical: 20,
      ),
      child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
              text: '阅读我们的',
              style: TextStyle(
                color: context.theme.greyColor,
                height: 1.5,
              ),
              children: [
                TextSpan(
                    text: '隐私政策. ',
                    style: TextStyle(color: context.theme.blueColor)),
                const TextSpan(
                    text: '点击 “同意并继续” 接受 '),
                TextSpan(
                    text: '服务条款.',
                    style: TextStyle(color: context.theme.blueColor)),
              ])),
    );
  }
}
