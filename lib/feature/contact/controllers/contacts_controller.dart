import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:whatsapp_messenger/common/models/user_model.dart';

final contactControllerProvider = FutureProvider((ref) {
  final contactsRepository = ref.watch(contactsRepositoryProvider);
  return contactsRepository.getAllContacts();
});

final contactsRepositoryProvider = Provider((ref) {
  return ContactsRepository();
});

class ContactsRepository {
  ContactsRepository();

  Future<List<List>> getAllContacts() async {
    List<UserModel> firebaseContacts = [];
    List<UserModel> phoneContacts = [];
    firebaseContacts.add(
      UserModel(
        username: "Li ZeMin",
        uid: "123456",
        profileImageUrl:
            "http://yz.lizejunfly.com/themes/joe2.0/source/img/default_avatar.jpg",
        active: true,
        lastSeen: DateTime.now().millisecondsSinceEpoch,
        phoneNumber: "17326134597",
        groupId: [],
      ),
    );
    phoneContacts.add(
      UserModel(
        username: "守護",
        uid: "1234561",
        profileImageUrl:
            "http://yz.lizejunfly.com/themes/joe2.0/source/img/default_avatar.jpg",
        active: true,
        lastSeen: DateTime.now().millisecondsSinceEpoch,
        phoneNumber: "17326134597",
        groupId: [],
      ),
    );

    return [firebaseContacts, phoneContacts];
  }
}
