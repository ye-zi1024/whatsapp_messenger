import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:whatsapp_messenger/common/models/user_model.dart';
import 'package:whatsapp_messenger/feature/auth/repository/auth_repository_api.dart';

final authControllerApiProvider = Provider(
  (ref) {
    final authRepositoryApi = ref.watch(authRepositoryApiProvider);
    return AuthControllerApi(
      authRepositoryApi: authRepositoryApi,
      ref: ref,
    );
  },
);

final userInfoAuthProvider = FutureProvider((ref) {
  final authController = ref.watch(authControllerApiProvider);
  return authController.getCurrentUserInfo();
});

class AuthControllerApi {
  final AuthRepositoryApi authRepositoryApi;
  final ProviderRef ref;

  AuthControllerApi({
    required this.authRepositoryApi,
    required this.ref,
  });

  void sendSmsCode({
    required BuildContext context,
    required String phoneNumber,
  }) {
    authRepositoryApi.sendSmsCode(
      context: context,
      phoneNumber: phoneNumber,
    );
  }

  void verifySmsCode({
    required BuildContext context,
    required String phoneNumber,
    required String smsCodeId,
    required String smsCode,
    required bool mounted,
  }) {
    authRepositoryApi.verifySmsCode(
      context: context,
      phoneNumber: phoneNumber,
      smsCodeId: smsCodeId,
      smsCode: smsCode,
      mounted: mounted,
    );
  }

  void saveUserInfoToFirestore({
    required String username,
    required var profileImageUrl,
    required BuildContext context,
    required bool mounted,
  }) {
    authRepositoryApi.saveUserInfoToFirestore(
      username: username,
      profileImage: profileImageUrl,
      ref: ref,
      context: context,
      mounted: mounted,
    );
  }

  Stream<UserModel> getUserPresenceStatus({required String uid}) {
    return authRepositoryApi.getUserPresenceStatus(uid: uid);
  }

  void updateUserPresence() {
    return authRepositoryApi.updateUserPresence();
  }

  Future<UserModel?> getCurrentUserInfo() async {
    UserModel? user = await authRepositoryApi.getCurrentUserInfo();
    return user;
  }
}
