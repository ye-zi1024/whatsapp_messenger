# whatsapp_messenger

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## 集成`photo_manager`遇到的问题
> Could not find kotlin-gradle-plugin-idea-proto-1.7.21.jar

### 解决步骤
- 找到flutter sdk `D:\soft\flutter\packages\flutter_tools\gradle`，修改两处
```
buildscript {
    repositories {
        // google()
        // mavenCentral()
		maven { url 'https://maven.aliyun.com/repository/google' }
		maven { url 'https://maven.aliyun.com/repository/jcenter' }
		maven { url 'https://maven.aliyun.com/nexus/content/groups/public' }
    }
    dependencies {
        /** When bumping, also update ndkVersion above, as well as the Android Gradle Plugin
         * version in ../lib/src/android/gradle_utils.dart.
         */
        classpath 'com.android.tools.build:gradle:7.3.0'
    }
}
```
```
class FlutterPlugin implements Plugin<Project> {
    // private static final String DEFAULT_MAVEN_HOST = "https://storage.googleapis.com";
	private static final String DEFAULT_MAVEN_HOST = "https://storage.flutter-io.cn";
    ...
}
```
- 修改 根目录下 `android -> build.gradle`
```
buildscript {
    ext.kotlin_version = '1.7.21'
    repositories {
        google()
        mavenCentral()
        maven {
            allowInsecureProtocol  true
            url 'https://maven.aliyun.com/repository/jcenter'
        }
        maven {
            allowInsecureProtocol  true
            url 'https://maven.aliyun.com/repository/google'
        }

        maven {
            allowInsecureProtocol  true
            url 'https://maven.aliyun.com/nexus/content/groups/public'
        }
        maven {
            allowInsecureProtocol  true
            url 'https://maven.aliyun.com/repository/public'
        }
        maven {
            allowInsecureProtocol  true
            url 'https://maven.aliyun.com/repository/jcenter'
        }
       maven { url 'https://maven.aliyun.com/repository/gradle-plugin' }
    }

    dependencies {
        classpath 'com.android.tools.build:gradle:7.3.0'
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"
    }
}

allprojects {
    repositories {
        google()
        mavenCentral()
        maven {
            allowInsecureProtocol  true
            url 'https://maven.aliyun.com/repository/jcenter'
        }
        maven {
            allowInsecureProtocol  true
            url 'https://maven.aliyun.com/repository/google'
        }

        maven {
            allowInsecureProtocol  true
            url 'https://maven.aliyun.com/nexus/content/groups/public'
        }
        maven {
            allowInsecureProtocol  true
            url 'https://maven.aliyun.com/repository/public'
        }
        maven {
            allowInsecureProtocol  true
            url 'https://maven.aliyun.com/repository/jcenter'
        }
       maven { url 'https://maven.aliyun.com/repository/gradle-plugin' }
    }
}
```